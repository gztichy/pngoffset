OPT_LEVEL = 2
DBG_LEVEL = 3

all:
	gcc -g${DBG_LEVEL} -O${OPT_LEVEL} -c src/endian.c -o src/endian.o
	gcc -g${DBG_LEVEL} -O${OPT_LEVEL} -c src/pngoffset.c -o src/pngoffset.o
	gcc -g${DBG_LEVEL} -O${OPT_LEVEL} src/endian.o src/pngoffset.o -o pngoffset
