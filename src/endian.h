#ifndef INC_ENDIAN__
#define INC_ENDIAN__

short   endianSwap_short(short x);
int     endianSwap_int(int x);
int64_t endianSwap_int64(int64_t x);

int     isBigEndian(void);

#endif  // INC_ENDIAN__
