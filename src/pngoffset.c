#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "endian.h"

#define ERR_NOFILE      1
#define ERR_NOTPNG      2
#define ERR_NOOFFSET    3
#define ERR_READERROR   4
#define ERR_INPUT       5
#define ERR_INTERNAL    255

#define PNG_HEADER      "\x89PNG\r\n\x001A\n"
#define GRAB_HEADER     "grAb"
#define IDAT_HEADER     "IDAT"

typedef struct {int x; int y; char found;} PNGOffset;
static char* progName;


void error(char* reason)
{
    fprintf(stderr, "%s: ERROR: %s\n", progName, reason);
}

void errorExit(int code, char* reason)
{
    error(reason);
    exit(code);
}

PNGOffset getPNGOffset(char* pngname)
{
    char pngHeader[]    = "--------";
    char chunkHeader[]  = "-------";
    char* result;
    int read, grabOff, resultInt;

    PNGOffset off = {0, 0, 0};
    FILE *png;

    png = fopen(pngname, "rb");

    if (png == NULL)
    {
        char reason[50];
        sprintf(reason, "could not open file \"%s\"", pngname);
        errorExit(ERR_READERROR, reason);
    }

    read = fread(pngHeader, 8, 1, png);

    if (strcmp(pngHeader, PNG_HEADER) != 0)
    {
        errorExit(ERR_NOTPNG, "not a PNG");
    }

    for (;;)
    {
        read = fread(chunkHeader, 7, 1, png);
        fseek(png, -3, SEEK_CUR);

        result = strstr(chunkHeader, IDAT_HEADER);
        if (feof(png) || strstr(chunkHeader, IDAT_HEADER) != NULL)
        {
            return off;
        }

        if (ferror(png))
        {
            errorExit(ERR_READERROR, "read error");
        }

        result = strstr(chunkHeader, GRAB_HEADER);
        if (result != NULL)
        {
            resultInt = result - chunkHeader;
            fseek(png, resultInt, SEEK_CUR);
            break;
        }
    }

    grabOff = ftello(png);

    read = fread(&off, 2, 4, png);

    if (!isBigEndian())
    {
        off.x = endianSwap_int(off.x);
        off.y = endianSwap_int(off.y);
    }

    off.found = 1;

    return off;
}


int main(int argc, char* argv[])
{
    int i;
    PNGOffset result;
    progName = argv[0];

    if (argc < 2)
    {
        errorExit(ERR_NOFILE, "no files given");
    }

    for (i = 1; i < argc; i++)
    {
        result = getPNGOffset(argv[i]);
        if (result.found)
        {
            printf("%s: (%d,%d)\n", argv[i], result.x, result.y);
        }
        else
        {
            printf("%s: not found\n", argv[i]);
        }
    }
}
